#!/usr/bin/python2
# -*- coding: utf-8 -*-

import requests
import json

import cgitb
cgitb.enable()
print "Content-Type: text/html;charset=utf-8\n"

src = "https://tinyhost.de/cgi/wetterbs.py"
try:
    weather = json.loads(requests.get(src, verify=False).text)
    print """<html><head></head><body><div>Current Weather Brunswick:</div> <br><div>"""
    print u"{}°C, {}%rH".format(weather["t_curr"]["value"], weather["h_curr"]["value"]).encode('utf-8') 
    print """</div></body></html>"""
except requests.exceptions.SSLError:
    print "SSLError!"
except requests.exceptions.ConnectionError:
    print "ConnError!"

