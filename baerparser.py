#!/usr/bin/python2
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import json
import os
import time

import cgitb
cgitb.enable()
print "Content-Type: application/json;charset=utf-8\n"

def tofloat(s, delim=" "):
    for sub in s.split(delim):
        try:
            v = float(sub.replace(",", "."))
            return v
        except ValueError:
            pass
    raise Exception("No float value in String: >" + s + "<") 

def pretty(value, unit, desc=""):
    d = {}
    d["value"] = tofloat(value)
    d["unit"] = unit
    d["desc"] = desc
    return d

def getNew():
    res = {}
    try:
        link = "https://www.wetterbaer.de/Daten/currenttm.html"
        fd = requests.get(link).text.encode("utf-8")

        res["source"] = link

        soup = BeautifulSoup(fd, 'html5lib')

        tr = soup.find_all("tr")

        d = tr[2].find_all("td")[2].get_text().strip()
        t = tr[3].find_all("td")[2].get_text().strip()
        res["timestamp"] = d + " " + t

        res["t_curr"] =  pretty((tr[8].find_all("td")[2].find_all("font")[-1].get_text()), u"°C", "Current Temperature (+2m)")
        res["t_max"]  =  pretty(tr[8].find_all("td")[3].find_all("font")[-1].get_text(), u"°C", "Maximum Tempeature today (+2m)")
        res["t_min"]  =  pretty(tr[8].find_all("td")[4].find_all("font")[-1].get_text(), u"°C", "Minimum Tempeature today (+2m)")

        res["h_curr"] =  pretty((tr[17].find_all("td")[2].find_all("font")[-1].get_text()), u"%", "Current Humidity")
        res["h_max"]  =  pretty(tr[17].find_all("td")[4].find_all("font")[-1].get_text(), u"%", "Maximum Humidity today")
        res["h_min"]  =  pretty(tr[17].find_all("td")[3].find_all("font")[-1].get_text(), u"%", "Minimum Humidity today")

        res["wind_curr"] =  pretty((tr[13].find_all("td")[2].find_all("font")[-1].get_text()), u"km/h", "Current wind speed")
        res["wind_max"]  =  pretty(tr[13].find_all("td")[3].find_all("font")[-1].get_text(), u"km/h", "Maximum wind speed today")

        res["windGust_curr"] =  pretty((tr[14].find_all("td")[2].find_all("font")[-1].get_text()), u"km/h", "Current wind gust tpeed")
        res["windGust_max"]  =  pretty(tr[14].find_all("td")[3].find_all("font")[-1].get_text(), u"km/h", "Maximum wind gust speed today")

	res["percipitation_rate"] = pretty(tr[28].find_all("td")[14].get_text(), u"l/m²/h", "Percipitation rate")

	res["air_pressure"] =  pretty(tr[21].find_all("td")[3].get_text()[7:], u"hPa", "Air pressure (at sea level)")
	res["air_pressure_change_3h"] = pretty(tr[21].find_all("td")[9].get_text(), u"ΔhPa/3h", "Air pressure change rate in 3 hours (at sea level")

        res["error"] = "No Error"
    except requests.exceptions.ConnectionError:
        res["error"] = "Error: Could not connect to " + link
    return res

cache = "cache.json"
timeout = 15*60

curr = None
if not os.path.exists(cache) or (os.path.getmtime(cache) + timeout) < int(time.time()):
    # cache expired. getting new info
    curr = getNew()
    with open(cache, "w") as f:
        json.dump(curr, f)
else:
    # cache valid. delivering old info
    with open(cache, "r") as f:
        curr = json.load(f)



print json.dumps(curr)
