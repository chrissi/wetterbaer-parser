#!/usr/bin/python2
# -*- coding: utf-8 -*-

import requests
import json
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

src = "/home/chris/Projekte/wetterbaer/cache.json"
try:
    with open(src) as fh:
        weather = json.load(fh)

    flags = ""
    if weather["percipitation_rate"]["value"] > 0: 
        flags += u" 🌧"

    print u"{}°C {}".format(weather["t_curr"]["value"], flags).encode('utf-8') 
except requests.exceptions.SSLError:
    print "SSLError!"
except requests.exceptions.ConnectionError:
    print "ConnError!"
except KeyError:
    print "JsonError"
except IOError:
    print "IOError"



